function m = bottom_left(N,n)
s = N([1,n]);
m = ones(n)*s;
end