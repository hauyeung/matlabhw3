function mm = mean_squares(nn)
mm = sum(nn.^2,1);
end