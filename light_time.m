function [l, distkm] = light_time(dist)
l = (dist*1.609 / 300000)/60;
distkm = dist*1.609;    
end