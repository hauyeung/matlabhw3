function x = odd_index(M)
x = M(1:2:size(M,1), 1:2:size(M,2));
end