function r = rich(A)
r = dot(A, [0.01,0.05,0.1,0.25]);
r = str2double(sprintf('%.2f',r));
end